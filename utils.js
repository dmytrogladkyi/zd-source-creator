"use strict";

const http = require('http');
const sec = require('./credentials.json');

module.exports = {
    SendSourcePost: function SendSourcePost(source, csrf) {
        var str = JSON.stringify(source);
        var post_options = {
            path: "/zoomdata/service/sources",
            host: "localhost",
            port: "8080",
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': csrf,
                'Authorization': 'Basic ' + new Buffer(sec.login + ':' + sec.password).toString('base64')
            }
        };

        var post_req = http.request(post_options, res => {
            res.setEncoding('utf8');
            var body = '';
            res.on('data', function(chunk) {
                body += chunk
            });

            res.on('end', () => {
                if (res.statusCode === 200) {
                    console.log("created source: ", source.name);
                } else {
                    console.warn(`${source.name} error: ${res.statusMessage}, ${body}`);
                }
            });

            res.on('error', () => {
                console.log('lol');
            });
        });

        post_req.on('error', (error) => {
            console.error('Error while creating source ', source.name, error);
        });

        post_req.write(str);
        console.log("sending: ", source.name);
        post_req.end();
    },

    getTypes: function getTypes(csrf) {
        const prom = new Promise((resolve, reject) => {
            var req = http.request({
                path: "/zoomdata/service/connections",
                host: "localhost",
                port: "8080",
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': csrf,
                    'Authorization': 'Basic ' + new Buffer(sec.login + ':' + sec.password).toString('base64')
                }
            }, function (res) {

                var body = '';
                
                res.on('data', (chunk) => {
                    body += chunk;
                });

                res.on('end', () => {
                    resolve(JSON.parse(body));
                });

                res.on('error', (err) => {console.log('error');});
            });
            req.end();
        });
        return prom;
    },

    getCsrf: function getCsrf() {
        return new Promise((resolve, reject) => {
            var req = http.request({
                path: "/zoomdata/login",
                host: "localhost",
                port: "8080",
                method: "GET"
            }, (res) => {
                var body = '';
                res.on('data', (chunk) => {
                    body += chunk;
                });

                res.on('end', () => {
                    const csrf = body.match(/value=\"([^\"]+)\"/)[1];
                    resolve(csrf);
                });

            });
            req.end();
        });

    }
};
