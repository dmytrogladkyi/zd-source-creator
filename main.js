"use strict";

const source_def = require('./source_def.json');
const Utils = require('./utils');

const mapArgs = {
    sourceName: 'My Flat Source ',
    startWith: 0,
    amount: 10
};

process.argv.forEach((val, index, array) => {
    console.log(`${index}: ${val}`);
    if (val.indexOf('--source-name') >= 0) {
        mapArgs.sourceName = val.split('=')[1] + ' ';
    }
    if (val.indexOf('--amount') >= 0) {
        mapArgs.amount = val.split('=')[1];
    }
    if (val.indexOf('--start-with') >= 0) {
        mapArgs.startWith = val.split('=')[1];
    }
});

console.log(JSON.stringify(mapArgs));

source_def.name = mapArgs.sourceName;


function startApp() {
    console.log('Starting app');
    var startWith = mapArgs.startWith;
    let amount = mapArgs.amount;
    let csrf = '';
    
    function createSource(prefix, connectionId) {
        console.log('conn id', connectionId);
        var newSourceDef = JSON.parse(JSON.stringify(source_def));
        newSourceDef.name = newSourceDef.name + prefix;
        newSourceDef.storageConfiguration.connectionId = connectionId;
        Utils.SendSourcePost(newSourceDef, csrf);    
    }

    function getRFSType(allConnections) {
        console.log('getting rfs type');
        return Promise.resolve(allConnections.filter(con => {
            return con.type === 'RFS' && con.subStorageType === 'CSV';
        })[0].id);
    }
    
    Utils.getCsrf().then((key) => {
        console.log(`Got csrf token: ${key}`);
        csrf = key;
        return csrf;
    }).then(Utils.getTypes, (err) => {
        console.log(err);
    }).then(getRFSType).then((connectionId) => {
        console.log(`rfs type found ${connectionId}`);
        console.log('Proceeding with source creation');
        const limit = Number(amount) + Number(startWith);
        for (let count = startWith; count < limit; count++) {
            createSource(count, connectionId);
        }
    });
}

startApp();





